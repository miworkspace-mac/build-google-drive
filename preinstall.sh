#!/bin/bash
# Look for Google Drive
if [ -d /Applications/Google\ Drive.app/ ]; then
  /bin/echo "`date`: Found depricated version of Google Drive. Removing."
  /usr/bin/osascript -e 'tell application "Google Drive" to quit'
  rm -Rf /Applications/Google\ Drive.app/
fi
exit 0