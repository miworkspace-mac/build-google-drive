#!/bin/bash -ex

# CONFIG
prefix="Google-Drive"
suffix=""
package_name="GoogleDrive"
icon_name=""
category="Productivity"
description="This update contains stability and security fixes for Google Drive"
#url="https://dl-ssl.google.com/drive/installgoogledrive.dmg"
url="https://dl.google.com/drive-file-stream/GoogleDrive.dmg""

# download it
curl -o app.dmg $url

# Build pkginfo  --postinstall_script=postinstall.sh
/usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg > app.plist

plist=`pwd`/app.plist

# Obtain version info
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`
echo Mounted on $mountpoint
pwd=`pwd`
version=`defaults read "$mountpoint/Google Drive.app/Contents/Info.plist" CFBundleVersion`
hdiutil detach "$mountpoint"

# Set version comparison key
/usr/libexec/PlistBuddy -c "Set :installs:0:version_comparison_key CFBundleVersion" app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "10.7.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${package_name}"
defaults write "${plist}" display_name "Google Drive"
defaults write "${plist}" version "${version}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" category "${category}"
defaults write "${plist}" description -string "${description}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.dmg   ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist

