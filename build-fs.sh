#!/bin/bash -ex

# CONFIG
prefix="Google-Drive"
suffix=""
package_name="GoogleDrive"
icon_name=""
category="Productivity"
description="This update contains stability and security fixes for Google Drive"
url="https://dl.google.com/drive-file-stream/GoogleDrive.dmg"

# download it
curl -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36' "${url}"

## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

pkg_in_dmg=$(ls -d $mountpoint/*.pkg)
cp -R "$pkg_in_dmg" app.pkg

/usr/sbin/pkgutil --expand-full app.pkg pkg
drive_pkg=$(ls -d pkg/G*_64.pkg)

mkdir -p build-root/Applications
#cp -R *.app build-root/Applications
cp -R "$drive_pkg"/Payload/*.app build-root/Applications

hdiutil detach "${mountpoint}"

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files) 
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist
## END EXAMPLE

plist=`pwd`/app.plist
version=`/usr/libexec/PlistBuddy -c "Print :installs:0:CFBundleVersion" "${plist}"`
minver=`/usr/libexec/PlistBuddy -c "Print :installs:0:minosversion" "${plist}"`

/usr/libexec/PlistBuddy -c "Set :installs:0:version_comparison_key CFBundleVersion" app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "13.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${package_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" RestartAction RequireLogout

# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${package_name}"`
defaults write "${plist}" display_name -string "${display_name}"


# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${package_name}"`
defaults write "${plist}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist

